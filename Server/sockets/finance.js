module.exports = (io, utils) => {

    let module = {};

    module.connection = (socket) => {
        
        let routes = [
            {"name": "requestSalaryOfThisMonth", "emitter": "Finance", "receiver": "Store"},
            {"name": "requestSalaryOfTheYear", "emitter": "Finance", "receiver": "Store"},
            {"name": "requestEmployeesCatalogue", "emitter": "Finance | Projet", "receiver": "Store"}
        ];

        for(let route of routes) {
            socket.on(route.name, function(data) {
                utils.addEntryToLogFile(route.emitter, route.receiver, route.name, data);
                io.emit("getLogs", utils.formatEntry(route.emitter, route.receiver, route.name, data));
                io.emit(route.name, data);
            });
        }
    }

    return module;
}
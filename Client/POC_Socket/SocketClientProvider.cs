﻿using Quobject.SocketIoClientDotNet.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Configuration;
using POC_Socket.Models;

namespace POC_Socket
{
    public static class SocketClientProvider
    {
        public static Socket CreateSocketClient(string socketServerUri, string clientIdentity)
        {

            var socketClient = IO.Socket(socketServerUri, new IO.Options()
            {
                ReconnectionAttempts = 2,
                ReconnectionDelay = 1000
            });

            socketClient.Emit("setIdentity", clientIdentity);

            socketClient.On(Socket.EVENT_CONNECT, (data) =>
            {
                Console.WriteLine("Successfully connected to socket");
            });

            socketClient.On(Socket.EVENT_CONNECT_ERROR, (data) =>
            {
                Console.WriteLine("Error while connecting to socket");
                Console.WriteLine(data.ToString());
            });

            return socketClient;
        }
    }
}

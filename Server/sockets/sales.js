module.exports = (io, utils) => {

    let module = {};

    module.connection = (socket) => {
        
        let routes = [
            {"name": "requestSellProduct", "emitter": "Ventes/achats", "receiver": "Store"},
            {"name": "addProductToStock", "emitter": "Ventes/achats", "receiver": "Store"},
            {"name": "requestProductsCatalogue", "emitter": "Ventes/achats", "receiver": "Store"},
            {"name": "sales", "emitter": "Ventes/achats", "receiver": "Finance"}
        ];

        for(let route of routes) {
            socket.on(route.name, function(data) {
                utils.addEntryToLogFile(route.emitter, route.receiver, route.name, data);
                io.emit("getLogs", utils.formatEntry(route.emitter, route.receiver, route.name, data));
                io.emit(route.name, data);
            });
        }
    }

    return module;
}
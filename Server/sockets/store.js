module.exports = (io, utils) => {

    let module = {};

    module.connection = (socket) => {
        
        let routes = [
            {"name": "productsCatalogue", "emitter": "Store", "receiver": "Ventes/achats"},
            {"name": "currentWorkForce", "emitter": "Store", "receiver": "Projet"},
            {"name": "salaryOfThisMonth", "emitter": "Store", "receiver": "Finance"},
            {"name": "salaryOfTheYear", "emitter": "Store", "receiver": "Finance"},
            {"name": "employeesCatalogue", "emitter": "Store", "receiver": "Finance | Projet"},
            {"name": "productsLowInStock", "emitter": "Store", "receiver": "Unkown receiver"},
            {"name": "requestProductsLowInStock", "emitter": "Unknown emitter", "receiver": "Store"},
            {"name": "sellProduct", "emitter": "Store", "receiver": "Ventes/achats"},
        ];

        for(let route of routes) {
            socket.on(route.name, function(data) {
                utils.addEntryToLogFile(route.emitter, route.receiver, route.name, data);
                io.emit("getLogs", utils.formatEntry(route.emitter, route.receiver, route.name, data));
                io.emit(route.name, data);
            });
        }
    }

    return module;
}
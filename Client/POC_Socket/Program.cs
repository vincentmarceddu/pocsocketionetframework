﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quobject.SocketIoClientDotNet.Client;
using Newtonsoft.Json;
using POC_Socket.Controllers;
using System.Configuration;

namespace POC_Socket
{
    class Program
    {
        static void Main(string[] args)
        {
            var socketServerUri = ConfigurationManager.AppSettings["SocketServerURI"];
            var clientIdentity = ConfigurationManager.AppSettings["ClientIdentity"];
            var socketClient = SocketClientProvider.CreateSocketClient(socketServerUri, clientIdentity);

            var userController = new UserController(socketClient);
            userController.GetUser();

            Console.ReadLine();
        }
    }
}

module.exports = (io, utils) => {

    let module = {};

    module.connection = (socket) => {
        
        let routes = [
            {"name": "requestCurrentWorkForce", "emitter": "Projet", "receiver": "Store"},
            {"name": "projectDetails", "emitter": "Projet", "receiver": "Ventes/achats"},
            {"name": "jobsDataPerMonth", "emitter": "Projet", "receiver": "Finance"}
        ];

        for(let route of routes) {
            socket.on(route.name, function(data) {
                utils.addEntryToLogFile(route.emitter, route.receiver, route.name, data);
                io.emit("getLogs", utils.formatEntry(route.emitter, route.receiver, route.name, data));
                io.emit(route.name, data);
            });
        }
    }

    return module;
}
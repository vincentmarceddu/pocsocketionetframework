document.getElementById("logs").scrollTop = 10000;
const socket = io.connect("ws://erp.thomaskleinhans.fr")
//const socket = io.connect("http://localhost")

socket.on('connection', (socket) => {
    console.info("Connected to the server...");
});

// socket.emit("handshake", "Time to go to bed...");

/*socket.on('requestProductsCatalogue', (data) => {
    console.log("Incoming request :", data);

    socket.emit("productsCatalogue", "This is a catalogue of products");
});*/

socket.on('getLogs', (data) => {
    console.info("New log entry", data);
    addLogEntry(data);
});

addLogEntry = (log) => {
    let logsContainer = document.getElementById("logs");
    let card = document.createElement("div");
    card.classList.add("card");

    let cardHeader = document.createElement("header");
    cardHeader.classList.add("card-header");
    
        let cardHeaderTitle = document.createElement("div");
        cardHeaderTitle.classList.add("card-header-title");
            let cardHeaderTitleParagraph = document.createElement("p");
                let cardHeaderTitleSpan = document.createElement("span");
                    let cardHeaderTitleSpanIcon = document.createElement("i");
                    cardHeaderTitleSpanIcon.classList.add("fas", "fa-feather-alt");
                let cardHeaderTitleIcon = document.createElement("i");
                cardHeaderTitleIcon.classList.add("fas", "fa-long-arrow-alt-right");
                let cardHeaderTitleSecondSpan = document.createElement("span");
                    let cardHeaderTitleSecondSpanIcon = document.createElement("i");
                    cardHeaderTitleSecondSpanIcon.classList.add("fas", "fa-envelope");
                let cardHeaderTitleThirdSpan = document.createElement("span");
					cardHeaderTitleThirdSpan.classList.add("lmr");

        let cardHeaderIcon = document.createElement("p");
        cardHeaderIcon.classList.add("card-header-icon");
            let cardHeaderIconSpan = document.createElement("span");
                let cardHeaderIconSpanIcon = document.createElement("i");
                cardHeaderIconSpanIcon.classList.add("fas", "fa-clock");

    let cardContent = document.createElement("div");
    cardContent.classList.add("card-content");
        let cardContentContent = document.createElement("div");
        cardContentContent.classList.add("content");

    cardHeaderTitleSpan.appendChild(cardHeaderTitleSpanIcon);
    cardHeaderTitleSpan.appendChild(document.createTextNode(log.emitter));

    cardHeaderTitleSecondSpan.appendChild(cardHeaderTitleSecondSpanIcon);
    cardHeaderTitleSecondSpan.appendChild(document.createTextNode(log.receiver));

    cardHeaderTitleThirdSpan.appendChild(document.createTextNode(`[${log.name}]`));

    cardHeaderTitleParagraph.appendChild(cardHeaderTitleSpan);
    cardHeaderTitleParagraph.appendChild(cardHeaderTitleIcon);
    cardHeaderTitleParagraph.appendChild(cardHeaderTitleSecondSpan);
    cardHeaderTitleParagraph.appendChild(cardHeaderTitleThirdSpan);
    
    cardHeaderIconSpan.appendChild(cardHeaderIconSpanIcon);
    cardHeaderIcon.appendChild(cardHeaderIconSpan).appendChild(document.createTextNode(log.date));

    cardHeader.appendChild(cardHeaderTitle).appendChild(cardHeaderTitleParagraph);
    cardHeader.appendChild(cardHeaderIcon);
    cardContent.appendChild(cardContentContent).appendChild(document.createTextNode(log.entry));

    card.appendChild(cardHeader);
    card.appendChild(cardContent); 

    logsContainer.prepend(card);
}
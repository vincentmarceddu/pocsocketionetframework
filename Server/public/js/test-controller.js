document.getElementById("logs").scrollTop = 10000;
const socket = io.connect("ws://erp.thomaskleinhans.fr")
// const socket = io.connect("http://localhost")

socket.on('connection', (socket) => {
    console.info("Connected to the server...");

});

socket.emit("requestProductsCatalogue", "I want the product catalogue !");

socket.on('productsCatalogue', (data) => {
    console.log("The catalogue of products's came :", data);
});

﻿using Newtonsoft.Json;
using POC_Socket.Abstraction;
using POC_Socket.Models;
using Quobject.SocketIoClientDotNet.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POC_Socket.Controllers
{
    class UserController : BaseController
    {
        public UserController(Socket socket) : base(socket)
        {
        }

        public override void InitializeListeners()
        {
            base._socketClient.On("setUser", (data) =>
            {
                Console.WriteLine("User set received !");

                User u = JsonConvert.DeserializeObject<User>(data.ToString());

                Console.WriteLine(u.ToString());
            });
        }

        public void GetUser()
        {
            base._socketClient.Emit("getUser");
        }
        
    }
}

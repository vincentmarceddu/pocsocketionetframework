var express = require('express');  
var app = express();  
var server = require('http').createServer(app);  
var io = require('socket.io')(server);

app.set('view engine', 'pug');
app.use(express.static(__dirname + '/node_modules'));
app.use(express.static('public'))
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", '*');
    res.header("Access-Control-Allow-Headers", 'Origin,X-Requested-With,Content-Type,Accept,content-type,application/json,Authorization');
    next();
  });

const utils = require("./utils");
const projectSockets = require('./sockets/project')(io, utils);
const financeSockets = require('./sockets/finance')(io, utils);
const salesSockets = require('./sockets/sales')(io, utils);
const storeSockets = require('./sockets/store')(io, utils);
const socketsModules = [projectSockets, financeSockets, salesSockets, storeSockets];

const user = {
    customFirstNameValue: "Thomas",
    LastName: "Kleinhans",
    Salary: 10000
};

app.get('/', function(req, res) {
    res.render('index', {logs: utils.getFormattedEntries()});
});

app.get('/test', function(req, res) {
    res.render('test');
});

io.on('connection', function(socket) {
    console.log('Client connected...');

    // Init every module we've defined containing routes
    for(socketModule of socketsModules) {
        socketModule.connection(socket);
    }

    //Test examples
    socket.on('setIdentity', function(data) {
        console.log(`Module connecté : ${data}`);
    });

    socket.on('getUser', function(){
        console.log("get user !");
        socket.emit("setUser", JSON.stringify(user));
    });

});

server.listen(3000);
const fs = require("fs");

exports.getEntriesFromLogFile = () => {
    return fs.readFileSync("./logs/logs").toString().split("\n");
};

exports.addEntryToLogFile = (emitter, receiver, name, entry) => {
    const jsonEntry = JSON.stringify(this.formatEntry(emitter, receiver, name, entry));
    fs.appendFile("./logs/logs", jsonEntry + "\r\n", () => {});
};

exports.addEntryToLogFileSync = (emitter, receiver, name, entry) => {
    const jsonEntry = JSON.stringify(this.formatEntry(emitter, receiver, name, entry));
    fs.appendFileSync("./logs/logs", jsonEntry + "\r\n", () => {});
};

exports.formatEntry = (emitter, receiver, name, entry) => {
    return {emitter, receiver, name, date: this.getFormattedDate(), entry}
};

exports.getFormattedEntries = () => {
    return this.getEntriesFromLogFile().reduce((acc, entry) => {
        return (entry ? [...acc, JSON.parse(entry)] : acc); 
    }, []).reverse();
}

exports.getFormattedDate = () => {
    return new Date().toISOString()
        .replace(/T/, ' ')
        .replace(/\..+/, '');
};
﻿using POC_Socket.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using Quobject.SocketIoClientDotNet.Client;
using System.Text;
using System.Threading.Tasks;

namespace POC_Socket.Controllers
{
    public abstract class BaseController : IBaseController
    {
        protected readonly Socket _socketClient;

        public BaseController(Socket socket)
        {
            this._socketClient = socket;
            InitializeListeners();
        }

        public abstract void InitializeListeners();
    }
}
